﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FluToDo.pballester.Model;
using System.Collections.ObjectModel;

namespace FluToDo.pballester.Data
{
    public interface IRestService
    {
        Task<ObservableCollection<ToDoItem>> GetListAsync();

        Task SaveItemAsync(ToDoItem item, bool isNewItem);

        Task DeleteItemAsync(string id);
    }
}