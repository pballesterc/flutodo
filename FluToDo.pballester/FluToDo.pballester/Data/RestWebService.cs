﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text;
using FluToDo.pballester.Model;
using System.Diagnostics;
using System.Collections.ObjectModel;

namespace FluToDo.pballester.Data
{
    public class RestWebService : IRestService
    { 
        private const string _cBaseUrl = "http://192.168.1.31:8080/api/todo";
        private Uri UriBase; 

        public RestWebService()
        {
        }

        public async Task<ObservableCollection<ToDoItem>> GetListAsync()
        {
            ObservableCollection<ToDoItem> Items;
            Items = new ObservableCollection<ToDoItem>();

            try
            {
                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync(_cBaseUrl);

                    if (response.IsSuccessStatusCode)
                    {
                        var content = await response.Content.ReadAsStringAsync();

                        Items = (ObservableCollection<ToDoItem>)Newtonsoft.Json.JsonConvert.DeserializeObject(content, typeof(ObservableCollection<ToDoItem>));
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

            return Items;
        }

        public async Task SaveItemAsync(ToDoItem item, bool isNewItem)
        {
            string url = isNewItem ? _cBaseUrl : _cBaseUrl + "/" + item.Key;
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(item);
            HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");

            try
            {
                using (var client = new HttpClient())
                {
                    if (isNewItem)
                        await client.PostAsync(url, content);
                    else
                        await client.PutAsync(url, content);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public async Task DeleteItemAsync(string key)
        {
            string url = _cBaseUrl + "/" + key;

            try
            {
                using (var client = new HttpClient())
                {
                    await client.DeleteAsync(url);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
    }
}
