﻿using System.Collections.Generic;
using FluToDo.pballester.Model;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace FluToDo.pballester.Data
{
    public class ToDoService
    {
        IRestService _service;

        public ToDoService(RestWebService service)
        {
            _service = service;
        }

        public Task<ObservableCollection<ToDoItem>> GetListAsync()
        {
            return _service.GetListAsync();
        }

        public Task SaveItemAsync(ToDoItem item, bool isNewItem)
        {
            return _service.SaveItemAsync(item, isNewItem);
        }

        public Task DeleteItemAsync(string id)
        {
            return _service.DeleteItemAsync(id);
        }
    }
}
