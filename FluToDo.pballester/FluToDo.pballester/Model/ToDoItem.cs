﻿using System;
using System.ComponentModel;

namespace FluToDo.pballester.Model
{
    public class ToDoItem : INotifyPropertyChanged
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged(nameof(Name)); }
        }

        public string Key { get; set; }

        private bool _isComplete;
        public bool IsComplete
        {
            get { return _isComplete; }
            set { _isComplete = value; OnPropertyChanged(nameof(IsComplete)); }
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion  
    }
}
