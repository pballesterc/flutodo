﻿using System;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;
using FluToDo.pballester.ViewModel;
using System.Threading.Tasks;
using FluToDo.pballester.Model;
using FluToDo.pballester.View;

namespace FluToDo.pballester
{
	public partial class MainPage : ContentPage
	{
        private ToDoListViewModel _viewModel;
        
        public MainPage()
		{
			InitializeComponent();

            Title = "FLUTODO - To do list";
		}

        protected override void OnAppearing()
        {
            this._viewModel = new ToDoListViewModel();

            Task.Run((Func<Task>)(async () =>
            {
                try
                {
                    await _viewModel.RefreshList();

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        this.BindingContext = _viewModel;

                    });
                }
                catch (Exception ex)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        DisplayAlert("FLUTODO", "Error displaying tasks. " + ex.Message, "OK");
                    });
                }
            }));

            base.OnAppearing();
        }

        private void lvToDoList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                ToDoItem item = (ToDoItem)e.SelectedItem;
                item.IsComplete = !item.IsComplete;

                lvToDoList.SelectedItem = null;

                _viewModel.UpdateItem(_viewModel.SelectedItem);
            }           
        }

        private void AddItemMenu_Activated(object sender, EventArgs e)
        {
            ((NavigationPage)this.Parent).PushAsync(new ToDoItemPage());
        }

        public void OnDelete(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            ToDoItem item = (ToDoItem)mi.CommandParameter;

            try
            {
                _viewModel.DeleteItem(item);

                DisplayAlert(item.Name, "ToDo item '" + item.Name + "' has been deleted correctly", "OK");
            }
            catch (Exception ex)
            {
                DisplayAlert(item.Name, "Error to delete item. " + ex.Message, "OK");
            }
        }
    }
}
