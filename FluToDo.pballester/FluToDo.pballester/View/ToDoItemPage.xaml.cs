﻿using FluToDo.pballester.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FluToDo.pballester.View
{
	public partial class ToDoItemPage : ContentPage
	{
        ToDoItemViewModel _viewModel;

		public ToDoItemPage ()
		{
			InitializeComponent ();

            Title = "New task";

            this._viewModel = new ToDoItemViewModel();
            this.BindingContext = _viewModel.Item;
		}

        protected override void OnAppearing()
        {
            this.txName.Focus();

            base.OnAppearing();
        }

        private void btSave_Clicked(object sender, EventArgs e)
        {
            if (Validate())
            {
                _viewModel.Save();

                ((NavigationPage)this.Parent).PopAsync();
            }
            else
                DisplayAlert("New task", "Name field is empty. Error to create a new task", "OK");
        }

        private bool Validate()
        {
            if (string.IsNullOrEmpty(txName.Text))
                return false;

            return true;
        }
    }
}