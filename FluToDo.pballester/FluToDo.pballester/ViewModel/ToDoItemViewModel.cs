﻿using FluToDo.pballester.Data;
using FluToDo.pballester.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace FluToDo.pballester.ViewModel
{
    class ToDoItemViewModel : INotifyPropertyChanged
    {
        private ToDoItem _item;
        public ToDoItem Item
        {
            get { return _item; }
            set { _item = value; OnPropertyChanged(nameof(Item)); }
        }

        private RestWebService _service;

        public ToDoItemViewModel()
        {
            _item = new ToDoItem();
            _service = new RestWebService();
        }

        public async void Save()
        {
            await _service.SaveItemAsync(Item, true);

        }

#region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
#endregion
    }
}
