﻿using System.Collections.Generic;
using System.ComponentModel;
using FluToDo.pballester.Model;
using FluToDo.pballester.Data;
using Xamarin.Forms;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace FluToDo.pballester.ViewModel
{
    public class ToDoListViewModel : INotifyPropertyChanged
    {
        #region Properties

        private ObservableCollection<ToDoItem> _todoList;
        public ObservableCollection<ToDoItem> ToDoList
        {
            get { return _todoList; }
            set
            {
                _todoList = value;
                //OnPropertyChanged(nameof(ToDoList));
            }
        }

        private ToDoItem _selectedItem;
        public ToDoItem SelectedItem
        {
            get { return _selectedItem; }
            set { _selectedItem = value; OnPropertyChanged(nameof(SelectedItem)); }
        }


        bool _isRefreshing;
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                _isRefreshing = value;
                OnPropertyChanged(nameof(IsRefreshing));
            }
        }

        private ToDoService _service;

        Command _refreshCommand;
        public Command RefreshCommand
        {
            get { return _refreshCommand; }
        }

        #endregion

        #region Methods

        public ToDoListViewModel()
        {
            _todoList = new ObservableCollection<ToDoItem>();
            _service = new ToDoService(new RestWebService());
            _refreshCommand = new Command(Refresh);
        }

        public void Selected(ToDoItem item)
        {
            _selectedItem = item;
        }

        public async void Refresh()
        {
            _todoList = await RefreshList();
        }

        public async Task<ObservableCollection<ToDoItem>> RefreshList()
        {
            ObservableCollection<ToDoItem> UpdateList = new ObservableCollection<ToDoItem>();

            IsRefreshing = true;
            UpdateList = await _service.GetListAsync();

            _todoList.Clear();
            foreach (ToDoItem item in UpdateList)
            {
                _todoList.Add(item);
            }

            IsRefreshing = false;

            return _todoList;
        }

        public async void UpdateItem(ToDoItem item)
        {
            await _service.SaveItemAsync(item, false);
        }

        public async void SaveItem(ToDoItem item)
        {
            await _service.SaveItemAsync(item, true);

            _todoList.Add(item);
        }

        public async void DeleteItem(ToDoItem item)
        {
            await _service.DeleteItemAsync(item.Key);

            _todoList.Remove(item);
        }

        #endregion  

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion  
    }
}
