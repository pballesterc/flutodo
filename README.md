# README #

Realizada prueba / test de una aplicación ToDo List multiplataforma en Xamarin Forms. La aplicación está probada y testeada en su versión Android cumpliendo con todas las funcionalidades escritas en el análisis de la app. También está operativa la versión para iOS.

Para las pruebas sobre dispositivo real y poder consumir correctamente los servicios web del servidor que se levanta sobre el mismo equipo, es necesario ejecutar Visual Studio de la WebApi en modo administrador para que al ejecutar el proceso lo haga contra localhost y la dirección ip. Sino se hace así al ejecutar el servicio de web services sólo se levantará sobre localhost y es más complicado ejecutar flutodo en el dispositivo real. Por ello, en el código aparecerá una dirección interna con las que he hecho las prueba que deberá ser cambiada para que funcione en otro equipo. Modificar línea 16 del flutodo.pballester.Data.RestWebService.

He subido también la webApi con una modificación sobre el ToDoController para que no hubiera conflicto al consumir un servicio web de tipo PUT.