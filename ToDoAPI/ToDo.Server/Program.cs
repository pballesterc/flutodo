﻿using System;
using Microsoft.Owin.Hosting;

namespace TodoApi
{
	public class Program
	{
		protected static void Main(string[] args)
		{
			var port = 8080;
			var url = $"http://localhost:{port}";

            StartOptions options = new StartOptions();
            options.Urls.Add("http://localhost:8001/");
            options.Urls.Add("http://192.168.1.31:8080/");
            using (WebApp.Start<Startup>(options))
			{
				Console.WriteLine($"Web Server is running at {url}.");
				Console.WriteLine("Press any key to quit.");
				Console.ReadLine();
			}
		}
	}
}
